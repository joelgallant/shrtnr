FROM node:10-alpine as build

WORKDIR /build
COPY . .

RUN yarn install --frozen-lockfile --production=false && \
  yarn clean && \
  yarn build

RUN rm -rf node_modules && \
  yarn install --frozen-lockfile --production

FROM node:10-alpine
RUN apk add --no-cache tini
ENTRYPOINT ["/sbin/tini", "--"]

ENV NODE_ENV production

COPY --from=build /build/node_modules node_modules
COPY --from=build /build .

ENV PORT 80
EXPOSE 80

CMD ["node", "./dist/index.js"]
