import fastify from 'fastify';
import { join } from 'path';
import { config, loadConfig } from '@lcdev/app-config';
import { readFile, outputFile, pathExists } from 'fs-extra';

const server = fastify();

server.register(require('fastify-formbody'))

async function main() {
  await loadConfig();

  if (!await pathExists(config.fileDatabase)) {
    await outputFile(config.fileDatabase, '{}');
  }

  const redirectsDatabase = JSON.parse((await readFile(config.fileDatabase)).toString());

  console.log('Redirects DB:', redirectsDatabase);

  const appHtml = await readFile(join(__dirname, '..', 'app.html'));

  server.get('/', async (req, reply) => {
    reply.type('text/html').send(appHtml);
  });

  server.get('/status', async (req, reply) => {
    return { ok: true };
  });

  server.get('/:link', async (req, reply) => {
    const { link } = req.params as { link: string };

    if (redirectsDatabase[link]) {
      reply.redirect(redirectsDatabase[link]);
    } else {
      reply.status(404).send('Not found');
    }
  });

  server.post('/:link', async (req, reply) => {
    if (req.headers.authorization === config.adminKey) {
      const { link } = req.params as { link: string };
      const { dest } = req.body as { dest: string };

      if (!dest) {
        return reply.status(400).send('No destination');
      }

      Object.assign(redirectsDatabase, {
        [link]: dest,
      });

      await outputFile(config.fileDatabase, JSON.stringify(redirectsDatabase));

      console.log('Redirects DB:', redirectsDatabase);

      return 'Success';
    }

    reply.status(401).send('Unauthorized');
  });

  server.delete('/:link', async (req, reply) => {
    if (req.headers.authorization === config.adminKey) {
      const { link } = req.params as { link: string };

      Object.assign(redirectsDatabase, {
        [link]: undefined,
      });

      await outputFile(config.fileDatabase, JSON.stringify(redirectsDatabase));

      console.log('Redirects DB:', redirectsDatabase);

      return 'Success';
    }

    reply.status(401).send('Unauthorized');
  });

  const port = process.env.PORT ? parseInt(process.env.PORT) : config.port;
  if (!port || Number.isNaN(port)) throw new Error('No port');

  await server.listen(port, '0.0.0.0');

  console.log(`Listening on :${port}`);
}

main().catch((error) => {
  console.error(error);
  setTimeout(() => process.exit(1), 0);
});
